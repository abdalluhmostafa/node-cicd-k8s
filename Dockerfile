FROM node:4.2
WORKDIR /src
COPY package.json /src
COPY server.js /src
WORKDIR /src
RUN npm install
EXPOSE 4000
CMD ["node", "/src/server.js"]